const UserController = module.exports;

const log4j = require('../utils/logger');
const LogUtils = require('../utils/LogUtils');

const UserService = require('../services/userService');

UserController.create = (req, res, next) => {
  const logName = 'createUser';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  logger.info(`Starts UserController.createUser : ${JSON.stringify(body)}`);

  return UserService.create(body, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.create : ${error.message}`);

      return next(error);
    });
};

UserController.changePassword = (req, res, next) => {
  const logName = 'changeUserPassword';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { body } = req;
  const { params } = req;

  logger.info(`Starts UserController.changePassword: ${JSON.stringify(params, body)}`);

  return UserService.changePassword(params.username, body.currentPassword, body.newPassword, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.changePasswod: ${error.message}`);

      return next(error);
    });
};

UserController.updateGroup = (req, res, next) => {
  const logName = 'updateGroup';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { params } = req;
  logger.info(`Starts UserController.updateGroup: ${JSON.stringify(params)}`);

  return UserService.addGroupToList(params.username, params.name, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in  UserController.updateGroup: ${error.message}`);

      return next(error);
    });
};

UserController.findUserPermissionsByUsername = (req, res, next) => {
  const logName = 'findUserPermissionsByUsername';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { params } = req;

  logger.info(`Starts UserController.findUserPermissionsByUsername: ${JSON.stringify(params)}`);

  return UserService.findUserPermissionsByUsername(params.username, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.findUserPermissionsByUsername: ${error.message}`);

      return next(error);
    });
};

UserController.activate = (req, res, next) => {
  const logName = 'activateUser';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { params } = req;

  logger.info(`Starts UserController.activate : ${JSON.stringify(params)}`);

  return UserService.activate(params.username, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.activate : ${error.message}`);

      return next(error);
    });
};

UserController.desactivate = (req, res, next) => {
  const logName = 'desactivateUser';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { params } = req;

  logger.info(`Starts UserController.desactivate : ${JSON.stringify(params)}`);

  return UserService.desactivate(params.username, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.desactivate : ${error.message}`);

      return next(error);
    });
};

UserController.addPermissionsToUser = (req, res, next) => {
  const logName = 'addPermissionsToUser';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { params, body } = req;
  logger.info(`Starts UserController.addPermissionsToUser: ${JSON.stringify(params)}, ${JSON.stringify(body)}`);

  return UserService.addPermissionsToUser(params.username, body.permissions, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.addPermissionsToUser: ${error.message}`);

      return next(error);
    });
};

UserController.removeGroupsToUser = (req, res, next) => {
  const logName = 'removeGroupToUser';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { params, body } = req;
  logger.info(`Starts UserController.removeGroupsToUser: ${JSON.stringify(params)}, ${JSON.stringify(body)}`);

  return UserService.removeGroupsToUser(params.username, body.groups, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.removeGroupsToUser: ${error.message}`);

      return next(error);
    });
};

UserController.removePermission = (req, res, next) => {
  const logName = 'removePermission';
  const logger = LogUtils.getLoggerWithId(log4j, logName);
  const { params } = req;
  logger.info(`Starts UserController.removePermission: ${JSON.stringify(params)}`);

  return UserService.removePermission(params.code, params.name, { logger, logName })
    .then((response) => res.send(response))
    .catch((error) => {
      logger.error(`Error in UserController.removePermission: ${error.message}`);

      return next(error);
    });
};
