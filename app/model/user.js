const mongoose = require('mongoose');

const { Schema } = mongoose;
const schema = new Schema({
  username: String,
  email: String,
  passwords: [{
    password: String,
    createdAt: Date,
    validUntil: Date,
    isCurrent: Boolean,
  }],
  code: String,
  entity: String,
  groups: [String],
  permissions: [String],
  mustChangePassword: Boolean,
  isActive: Boolean,
});

module.exports = mongoose.model('User', schema, 'users');
