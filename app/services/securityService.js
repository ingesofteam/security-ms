const SecurityService = module.exports;
const jwt = require('jsonwebtoken');
const UserRepository = require('../repositories/userRepository');
const UserService = require('./userService');

const Encrypter = require('../utils/encrypter');

const { UnauthorizedError } = require('../utils/ErrorHandlerMiddleware');

const {
  JWT_SECRET,
  TOKEN_EXPIRATION = '1d',
} = process.env;

SecurityService.login = async (username, pass) => {
  const user = (await UserRepository.findByUsername(username))[0];

  if (!user) throw new UnauthorizedError('Invalid credentials');

  const currentPass = user.passwords.find((it) => it.isCurrent === true).password;

  if (!await Encrypter.comparePassword(pass, currentPass)) {
    throw new UnauthorizedError('Invalid credentials');
  }

  const permissions = await UserService.findUserPermissionsByUsername(username);

  const payload = {
    username,
    groups: permissions.groups,
    permissions: permissions.permissions,
  };

  const token = jwt.sign(payload, JWT_SECRET, { expiresIn: TOKEN_EXPIRATION });

  return {
    token,
  };
};

SecurityService.validateToken = (token) => {
  try {
    const payload = jwt.verify(token, JWT_SECRET);

    return Promise.resolve(payload);
  } catch (error) {
    throw new UnauthorizedError('Invalid token');
  }
};
