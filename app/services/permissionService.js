const PermissionService = module.exports;

const permissionService = module.exports;
const { NotFoundError, BusinessError } = require('../utils/ErrorHandlerMiddleware');
const permissionRepository = require('../repositories/permissionRepository');
const groupRepository = require('../repositories/groupRepository');
const userRepository = require('../repositories/userRepository');
const { BadRequestError } = require('../utils/ErrorHandlerMiddleware');
const PermissionRepository = require('../repositories/permissionRepository');

PermissionService.create = async (permission) => {
  const findPermission = await PermissionRepository.findByName(permission.name.toLowerCase());

  if (findPermission.length > 0) {
    throw new BusinessError('The name already exist');
  }
  if (permission.name.match(' ')) {
    throw new BusinessError('The name can not has spaces');
  }
  if (!(/^[a-zA-Z]+$/.test(permission.name))) {
    throw new BadRequestError('The name can not has numbers');
  }
  permission.name.toLowerCase();

  return PermissionRepository.create(permission);
};

permissionService.list = async (name) => {
  const permission = (await permissionRepository.findByName(name))[0];
  if (!permission) {
    throw new NotFoundError('permission does not exist');
  }

  return permission;
};

permissionService.delete = async (name) => {
  const permission = (await permissionRepository.findByName(name))[0];
  if (!permission) {
    throw new NotFoundError('permission does not exist');
  }
  const user = (await userRepository.findUserPermissionByName(name))[0];
  if (user) {
    throw new BusinessError('An user has this permission assigned');
  }
  const group = (await groupRepository.findGroupPermissionByName(name))[0];
  if (group) {
    throw new BusinessError('A group has this permission assigned');
  }
  await permissionRepository.deleteByName(name);
};
