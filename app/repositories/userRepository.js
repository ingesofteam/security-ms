const UserRepository = module.exports;

const User = require('../model/user');

UserRepository.create = (user) => {
  const entity = new User(user);

  return entity.save();
};

UserRepository.findByCode = (code) => {
  const query = User.where({ code });

  return query.exec();
};

UserRepository.findByUsername = (username) => {
  const query = User.where({ username });

  return query.exec();
};

UserRepository.activateByName = (username) => {
  const query = User.findOneAndUpdate({ username }, { isActive: true });

  return query.exec();
};

UserRepository.deactivateByName = (username) => {
  const query = User.findOneAndUpdate({ username }, { isActive: false });

  return query.exec();
};

UserRepository.listUsersFromGroup = (group) => {
  const query = User.find({ groups: group }, {
    username: 1, groups: 1, permissions: 1, code: 1, isActive: 1,

  });

  return query.exec();
};

UserRepository.edit = (user) => {
  const entity = new User(user);

  return entity.save();
};

UserRepository.findUserGroupByName = (name) => {
  const query = User.find({ groups: name });

  return query.exec();
};

UserRepository.findUserPermissionByName = (name) => {
  const query = User.find({ permissions: name });

  return query.exec();
};
