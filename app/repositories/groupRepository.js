const GroupRepository = module.exports;

const Group = require('../model/group');

GroupRepository.create = (group) => {
  const entity = new Group(group);

  return entity.save();
};

GroupRepository.findByName = (name) => {
  const query = Group.where({ name });

  return query.exec();
};

GroupRepository.edit = (group) => {
  const entity = new Group(group);

  return entity.save();
};

GroupRepository.findGroupsByNames = (names) => {
  const query = Group.where({ name: { $in: names } });

  return query.exec();
};

GroupRepository.delete = (name) => {
  const query = Group.deleteOne({ name });

  return query.exec();
};

GroupRepository.findGroupPermissionByName = (name) => {
  const query = Group.find({ permissions: name });

  return query.exec();
};

GroupRepository.findAll = () => {
  const query = Group.where({});

  return query.exec();
};
