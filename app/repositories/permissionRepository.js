const permissionRepository = module.exports;

const Permission = require('../model/permission');

permissionRepository.findByName = (name) => {
  const query = Permission.where({ name });

  return query.exec();
};

permissionRepository.findPermissionsByNames = (names) => {
  const query = Permission.where({ name: { $in: names } });

  return query.exec();
};
permissionRepository.create = (permission) => {
  const entity = new Permission(permission);

  return entity.save();
};

permissionRepository.deleteByName = (name) => {
  const entity = Permission.where({ name });
  const query = Permission.deleteOne(entity);

  return query.exec();
};
