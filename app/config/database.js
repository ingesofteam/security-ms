const Database = module.exports;

const mongoose = require('mongoose');

const {
  MONGO_URL = 'mongodb://localhost:27017/test',
} = process.env;

Database.connect = async () => {
  await mongoose.connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });

  const db = mongoose.connection;

  db.on('error', console.error.bind(console, 'connection error:'));

  db.once('open', () => {
    console.log('Mongo DBConnection Successful!');
  });
};
