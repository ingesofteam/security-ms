const Encrypter = module.exports;
const bcrypt = require('bcrypt');

const saltRounds = 12;

Encrypter.encryptPassword = (password) => bcrypt.hashSync(password, saltRounds);

Encrypter.comparePassword = (password, hashedPassword) => bcrypt.compare(password, hashedPassword);
