const DBHelper = module.exports;
const Group = require('../app/model/group');
const Permission = require('../app/model/permission');
const User = require('../app/model/user');

DBHelper.clearAll = async () => {
  await User.remove({});
  await User.deleteMany({});
  await Group.remove({});
  await Permission.remove({});
  await Group.deleteMany({});
};
