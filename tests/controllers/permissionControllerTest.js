const chai = require('chai');

const { assert } = chai;
const chaiHttp = require('chai-http');
const app = require('../../index');
const DBHelper = require('../DBHelper');
const permissionRepository = require('../../app/repositories/permissionRepository');
const userRepository = require('../../app/repositories/userRepository');
const groupRepository = require('../../app/repositories/groupRepository');

const API = '/api/security-ms';
chai.use(chaiHttp);

describe('Permission CREATE flows', () => {
  beforeEach(async () => {
    await DBHelper.clearAll();
  });

  it('create permission validation success', async () => chai
    .request(app)
    .post(`${API}/permissions`)
    .send({
      name: 'firstpermission',
    })
    .then(async (response) => {
      const { status } = response;
      assert.strictEqual(status, 200);

      const permission = await permissionRepository.findByName('firstpermission');

      assert.isNotEmpty(permission);
      assert.strictEqual(permission[0].name, 'firstpermission');
    }));

  it('create permission validation name already exist', async () => {
    const data = {
      name: 'permissionstudent',
    };
    await permissionRepository.create(data);

    return chai
      .request(app)
      .post(`${API}/permissions`)
      .send(data).then(async (response) => {
        const { body, status } = response;
        assert.equal(status, 412);
        assert.deepEqual(body, {
          error: {
            message: 'The name already exist',
            code: 412,
          },
        });
      });
  });

  it('list permission test', async () => {
    const permissionData = {
      name: 'sara',
    };
    await permissionRepository.create(permissionData);

    return chai
      .request(app)
      .get(`${API}/permissions/${permissionData.name}`)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);

        const permission = await permissionRepository.findByName('sara');

        assert.isNotNull(permission);
        assert.equal(permission[0].name, 'sara');
      });
  });

  it('list permission test and permission does not exist', async () => chai
    .request(app)
    .get(`${API}/permissions/sara`)
    .then(async (response) => {
      const { body, status } = response;
      assert.strictEqual(status, 404);
      assert.deepEqual(body, {
        error: {
          message: 'permission does not exist',
          code: 404,
        },
      });
    }));

  it('delete permission test', async () => {
    const permissionData = {
      name: 'sara',
    };
    await permissionRepository.create(permissionData);
    const userData = {
      username: 'sara',
      email: 'sara@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };
    await userRepository.create(userData);

    const groupData = {
      name: 'group',
      permissions: [],
    };
    await groupRepository.create(groupData);

    return chai
      .request(app)
      .delete(`${API}/permissions/${permissionData.name}`)
      .then(async (response) => {
        const { status } = response;
        assert.strictEqual(status, 200);

        const permission = await permissionRepository.findByName('sara');

        assert.isEmpty(permission);
      });
  });

  it('delete permission test and is in user', async () => {
    const permissionData = {
      name: 'sara',
    };
    await permissionRepository.create(permissionData);
    const userData = {
      username: 'sara',
      email: 'sara@gmail.com',
      passwords: [],
      groups: [],
      permissions: ['sara'],
      mustChangePassword: true,
      isActive: true,
    };
    await userRepository.create(userData);

    const groupData = {
      name: 'group',
      permissions: [],
    };
    await groupRepository.create(groupData);

    return chai
      .request(app)
      .delete(`${API}/permissions/${permissionData.name}`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 412);
        assert.deepEqual(body, {
          error: {
            message: 'An user has this permission assigned',
            code: 412,
          },
        });
      });
  });

  it('create permission validation spaces in name', async () => chai
    .request(app)
    .post(`${API}/permissions`)
    .send({
      name: 'permission professor',
    })
    .then(async (response) => {
      const { body, status } = response;
      assert.strictEqual(status, 412);
      assert.deepEqual(body, {
        error: {
          message: 'The name can not has spaces',
          code: 412,
        },
      });
    }));

  it('create permission validation number in name', async () => chai
    .request(app)
    .post(`${API}/permissions`)
    .send({
      name: 'permission123',
    })
    .then(async (response) => {
      const { body, status } = response;
      assert.strictEqual(status, 400);
      assert.deepEqual(body, {
        error: {
          message: 'The name can not has numbers',
          code: 400,
        },
      });
    }));

  it('delete permission test and is in group', async () => {
    const permissionData = {
      name: 'sara',
    };
    await permissionRepository.create(permissionData);
    const userData = {
      username: 'sara',
      email: 'sara@gmail.com',
      passwords: [],
      groups: [],
      permissions: [],
      mustChangePassword: true,
      isActive: true,
    };
    await userRepository.create(userData);

    const groupData = {
      name: 'group',
      permissions: ['sara'],
    };
    await groupRepository.create(groupData);

    return chai
      .request(app)
      .delete(`${API}/permissions/${permissionData.name}`)
      .then(async (response) => {
        const { body, status } = response;
        assert.strictEqual(status, 412);
        assert.deepEqual(body, {
          error: {
            message: 'A group has this permission assigned',
            code: 412,
          },
        });
      });
  });
});
