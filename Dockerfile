FROM node:16-alpine3.11

RUN mkdir /app

COPY . /app

WORKDIR /app

RUN npm i
 
CMD node index.js